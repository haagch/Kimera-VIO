/* ----------------------------------------------------------------------------
 * Copyright 2017, Massachusetts Institute of Technology,
 * Cambridge, MA 02139
 * All Rights Reserved
 * Authors: Luca Carlone, et al. (see THANKS for the full author list)
 * See LICENSE for the license information
 * -------------------------------------------------------------------------- */

/**
 * @file   MonoVisionImuFrontend.cpp
 * @brief  Class describing a monocular tracking Frontend
 * @author Marcus Abate
 */

#include <memory>

#include "kimera-vio/frontend/MonoVisionImuFrontend-definitions.h"
#include "kimera-vio/frontend/MonoVisionImuFrontend.h"

DEFINE_bool(log_mono_matching_images,
            false,
            "Display/Save mono tracking rectified and unrectified images.");

namespace VIO {

MonoVisionImuFrontend::MonoVisionImuFrontend(
    const ImuParams& imu_params,
    const ImuBias& imu_initial_bias,
    const MonoFrontendParams& frontend_params,
    const Camera::ConstPtr& camera,
    DisplayQueue* display_queue,
    bool log_output)
    : VisionImuFrontend(imu_params, imu_initial_bias, display_queue, log_output),
    mono_frame_k_(nullptr),
    mono_frame_km1_(nullptr),
    mono_frame_lkf_(nullptr),
    keyframe_R_ref_frame_(gtsam::Rot3::identity()),
    feature_detector_(nullptr),
    mono_camera_(camera),
    frontend_params_(frontend_params) {
  CHECK(mono_camera_);

  tracker_ = VIO::make_unique<Tracker>(frontend_params_, mono_camera_, display_queue);

  feature_detector_ = VIO::make_unique<FeatureDetector>(
      frontend_params_.feature_detector_params_);

  if (VLOG_IS_ON(1)) tracker_->tracker_params_.print();
}

MonoVisionImuFrontend::~MonoVisionImuFrontend() {
  LOG(INFO) << "MonoVisionImuFrontend destructor called.";
}

MonoFrontendOutput::UniquePtr MonoVisionImuFrontend::bootstrapSpinMono(
    MonoFrontendInputPayload::UniquePtr&& input) {
  CHECK(input);

  // Initialize members of the Frontend
  processFirstFrame(input->getFrame());

  // Initialization done, set state to nominal
  frontend_state_ = FrontendState::Nominal;

  // Create mostly invalid output
  CHECK(mono_frame_lkf_);
  CHECK(mono_camera_);
  return VIO::make_unique<MonoFrontendOutput>(mono_frame_lkf_->isKeyframe_,
                                              nullptr,
                                              TrackingStatus::DISABLED,
                                              gtsam::Pose3::identity(),  // no stereo!
                                              mono_camera_->getBodyPoseCam(),
                                              *mono_frame_lkf_,
                                              nullptr,
                                              input->getImuAccGyrs(),
                                              cv::Mat(),
                                              getTrackerInfo());
}

MonoFrontendOutput::UniquePtr MonoVisionImuFrontend::nominalSpinMono(
    MonoFrontendInputPayload::UniquePtr&& input) {
  // For timing
  // std::cout << "\033[38;5;206m>>> " << "MonoVisionImuFrontend::nominalSpinMono()" << "\033[0m\n";
  utils::StatsCollector timing_stats_frame_rate(
      "VioFrontend Frame Rate [ms]");
  utils::StatsCollector timing_stats_keyframe_rate(
      "VioFrontend Keyframe Rate [ms]");
  auto start_time = utils::Timer::tic();

  const Frame& mono_frame_k = input->getFrame();
  const auto& k = mono_frame_k.id_;
  VLOG(1) << "------------------- Processing frame k = " << k
          << "--------------------";

  if (VLOG_IS_ON(10)) input->print();

  // XXX: Preintegration
  // There is a comment in StereoVisionImuFrontend that might be useful
  // Preintegration input: buffered imu timestamps and acc&gyr measurements
  // Preintegration output:
  // - pim_ is updated
  // - pim is a copy of the updated pim_ that will be passed to the backend
  // - pim->deltaRij() will be used for tracking
  auto tic_full_preint = utils::Timer::tic();
  const ImuFrontend::PimPtr& pim = imu_frontend_->preintegrateImuMeasurements(
      input->getImuStamps(), input->getImuAccGyrs()); // XXX
  CHECK(pim);
  const gtsam::Rot3 body_R_cam =
      mono_camera_->getBodyPoseCam().rotation();
  const gtsam::Rot3 cam_R_body = body_R_cam.inverse();
  // XXX: From StereioVisionImuFrontend:
  // "Relative rotation of the left cam rectified from the last keyframe to the
  // curr frame. pim.deltaRij() corresponds to bodyLkf_R_bodyK_imu"
  // Translation: pim->deltaRij is the rotation difference between last keyframe
  // and current frame of the "body" (the IMU)
  // while this next variable is the rotation delta of the camera
  // Note: think about an IMU with an attached camera that is a bit tilted
  // XXX: the word "rectified" might be important, I'm considering it just means
  // "from the camera reference space" but there is a Image Rectification
  // wikipedia article.
  // EDIT: Rectified might be the same as "undistorted"
  // XXX: R just means Rot
  // EDIT: deltaRij is the delta since the last keyframe
  // XXX: camLrectLkf_R_camLrectK_imu varibable name means "imu Rotation from
  // LastKeyFrame RECTified Left CAMera to frame K RECTified Left CAMera"
  gtsam::Rot3 camLrectLkf_R_camLrectK_imu =
      cam_R_body * pim->deltaRij() * body_R_cam;

  // std::cout << "\033[38;5;206m>>> " << "mono_camera_ bodyPoseCam = " << mono_camera_->getBodyPoseCam() << "\033[0m\n";
  // std::cout << "\033[38;5;206m>>> " << "body_R_cam = " << body_R_cam << "\033[0m\n";
  // std::cout << "\033[38;5;206m>>> " << "cam_R_body = " << cam_R_body << "\033[0m\n";
  // std::cout << "\033[38;5;206m>>> " << "pim->deltaRij() = " << pim->deltaRij() << "\033[0m\n";
  // std::cout << "\033[38;5;206m>>> " << "pim_->deltaRij() = " << pim_->deltaRij() << "\033[0m\n";
  // std::cout << "\033[38;5;206m>>> " << "camLrectLkf_R_camLrectK_imu = " << camLrectLkf_R_camLrectK_imu << "\033[0m\n";
  // std::cout << "\033[38;5;206m>>> " << "experiment is the same? = " << body_R_cam * pim->deltaRij() << "\033[0m\n";

  if (VLOG_IS_ON(10)) {
    body_R_cam.print("body_R_cam");
    camLrectLkf_R_camLrectK_imu.print("camLrectLkf_R_camLrectK_imu");
  }

  /////////////////////////////// TRACKING /////////////////////////////////////
  // XXX: Tracking input:
  // - cam rotation delta from last keyframe
  // - mono_frame_k: is a Frame, contains the img cv matrix and has container
  // info that I think will be updated right now by the tracker like landmark
  // info, is_keyframe and camparams
  // Tracking output:
  // - cv::Mat feature_tracks (it is just the display image for the --visualize_feature_tracks flag)
  // - StatusMonoMeasurements:
  //   - on keyframe: pair of TrackerStatusSummary (status and tracker (body) position)
  //     and a list of valid landmarkIDs with their corresponding undistorted 2d keypoints (shouldn't them be 3d points?)
  //   - on regular frame: TrackerStatusSummary has just an invalid status and nothing else
  //     and the list of landmarkIDs is empty
  //
  VLOG(10) << "Starting processFrame...";
  cv::Mat feature_tracks;
  StatusMonoMeasurementsPtr status_mono_measurements = processFrame(
      mono_frame_k, camLrectLkf_R_camLrectK_imu, &feature_tracks);
  // XXX: note that mono_frame_k_ is null because it was reset()ed in processFrame
  CHECK(!mono_frame_k_);  // We want a nullptr at the end of the processing.
  VLOG(10) << "Finished processStereoFrame.";
  //////////////////////////////////////////////////////////////////////////////

  if (VLOG_IS_ON(5))
    MonoVisionImuFrontend::printStatusMonoMeasurements(*status_mono_measurements);

  if (mono_frame_km1_->isKeyframe_) { // XXX: Careful, a new keyframe does not execute this code, but the frame after does
    CHECK_EQ(mono_frame_lkf_->timestamp_, mono_frame_km1_->timestamp_);
    CHECK_EQ(mono_frame_lkf_->id_, mono_frame_km1_->id_);
    CHECK(!mono_frame_k_);
    CHECK(mono_frame_lkf_->isKeyframe_);
    VLOG(1) << "Keyframe " << k
            << " with: " << status_mono_measurements->second.size()
            << " smart measurements";

    ////////////////// DEBUG INFO FOR FRONT-END ////////////////////////////////
    if (logger_) { // XXX: logged once per keyframe
      logger_->logFrontendStats(mono_frame_lkf_->timestamp_,
                                getTrackerInfo(),
                                tracker_status_summary_,
                                mono_frame_km1_->getNrValidKeypoints());
      // TODO(marcus): Last arg is usually stereo, need to refactor logger
      // to not require that.
      // XXX: The important predicted logging pose happens here
      logger_->logFrontendRansac(mono_frame_lkf_->timestamp_,
                                 tracker_status_summary_.lkf_T_k_mono_,
                                 gtsam::Pose3::identity());
    }
    //////////////////////////////////////////////////////////////////////////////

    // Reset integration; the later the better.
    VLOG(10) << "Reset IMU preintegration with latest IMU bias.";
    // XXX: Note that this is only happening just _after_ the keyframe
    // it resets the pim to zero BUT keeps the estimated bias until now
    imu_frontend_->resetIntegrationWithCachedBias();

    // Record keyframe rate timing
    timing_stats_keyframe_rate.AddSample(utils::Timer::toc(start_time).count());

    // Return the output of the Frontend for the others.
    VLOG(2) << "Frontend output is a keyframe: pushing to output callbacks.";
    return VIO::make_unique<MonoFrontendOutput>(
        true,
        status_mono_measurements,
        TrackingStatus::DISABLED,  // This is a stereo status only // XXX: This is weird tbh
        gtsam::Pose3::identity(),  // don't pass stereo pose to Backend! // XXX: weird as well
        mono_camera_->getBodyPoseCam(),
        *mono_frame_lkf_,  //! This is really the current keyframe in this if
        pim,
        input->getImuAccGyrs(),
        feature_tracks,
        getTrackerInfo());
  } else {
    // Record frame rate timing
    timing_stats_frame_rate.AddSample(utils::Timer::toc(start_time).count());

    VLOG(2) << "Frontend output is not a keyframe. Skipping output queue push.";
    return VIO::make_unique<MonoFrontendOutput>(
        false,
        status_mono_measurements, // XXX: This will be basically empty on non-keyframes
        TrackingStatus::DISABLED,  // This is a stereo status only // XXX: again this is weird tbh
        gtsam::Pose3::identity(),  // don't pass stereo pose to Backend! // XXX: again weird as well
        mono_camera_->getBodyPoseCam(),
        *mono_frame_lkf_,  //! This is really the current keyframe in this if
        pim,
        input->getImuAccGyrs(),
        feature_tracks,
        getTrackerInfo());
  }
}

void MonoVisionImuFrontend::processFirstFrame(const Frame& first_frame) {
  VLOG(2) << "Processing first mono frame \n";
  mono_frame_k_ = std::make_shared<Frame>(first_frame);
  mono_frame_k_->isKeyframe_ = true;
  last_keyframe_timestamp_ = mono_frame_k_->timestamp_;

  CHECK_EQ(mono_frame_k_->keypoints_.size(), 0)
      << "Keypoints already present in first frame: please do not extract"
         " keypoints manually";

  CHECK(feature_detector_);
  feature_detector_->featureDetection(mono_frame_k_.get());

  // Undistort keypoints:
  mono_camera_->undistortKeypoints(mono_frame_k_->keypoints_,
                                   &mono_frame_k_->keypoints_undistorted_);

  // TODO(marcus): get 3d points if possible?
  mono_frame_km1_ = mono_frame_k_;
  mono_frame_lkf_ = mono_frame_k_;
  mono_frame_k_.reset();
  ++frame_count_;

  imu_frontend_->resetIntegrationWithCachedBias();
}

StatusMonoMeasurementsPtr MonoVisionImuFrontend::processFrame(
    const Frame& cur_frame,
    const gtsam::Rot3& keyframe_R_cur_frame,
    cv::Mat* feature_tracks) {
  VLOG(1) << "===================================================\n"
          << "processing frame: " << cur_frame.id_ << " at time "
          << cur_frame.timestamp_ << " empirical framerate (sec): "
          << UtilsNumerical::NsecToSec(cur_frame.timestamp_ -
                                       mono_frame_km1_->timestamp_)
          << " (timestamp diff: "
          << cur_frame.timestamp_ - mono_frame_km1_->timestamp_ << ")";
  auto start_time = utils::Timer::tic();

  // XXX: Holy wacamole, cur_frame is never used and just straight out deep copied here
  mono_frame_k_ = std::make_shared<Frame>(cur_frame);

  VLOG(2) << "Starting feature tracking...";
  // XXX: keyframe_R_ref_frame_ stores the previous keyframe_R_cur_frame (relative rotation between frames)
  // or the identity if previous frame was a keyframe
  // so ref_frame_R_cur_frame is the composition of that relative rotation and the current relative rotation
  // see StereoVisionImuFrontend.keyframe_R_ref_frame for more info
  // EDIT: ref_frame_R_cur_frame is passed to feature_tracking and that is passed to OpticalFlowPredictor
  // predictSparseFlow() which says that it is just the "rotation from camera 1 to camera 2"
  // so basically I'm pretty lost on what .compose() does but it seems to only let the delta rot between ref and cur
  // XXX: Something else that is useful to know is that x_R_y naming convention
  // seems to mean "the rotation needed transform x to y"
  gtsam::Rot3 ref_frame_R_cur_frame =
      keyframe_R_ref_frame_.inverse().compose(keyframe_R_cur_frame);
  tracker_->featureTracking(mono_frame_km1_.get(), // <<<
                            mono_frame_k_.get(),
                            ref_frame_R_cur_frame);
  // XXX: feature_tracks is just the image
  if (feature_tracks) {
    *feature_tracks = tracker_->getTrackerImage(*mono_frame_lkf_,
                                                *mono_frame_k_);
  }
  VLOG(2) << "Finished feature tracking.";

  // TODO(marcus): need another structure for monocular slam
  tracker_status_summary_.kfTrackingStatus_mono_ = TrackingStatus::INVALID;
  tracker_status_summary_.kfTrackingStatus_stereo_ = TrackingStatus::DISABLED;

  MonoMeasurements smart_mono_measurements;

  const bool max_time_elapsed =
      mono_frame_k_->timestamp_ - last_keyframe_timestamp_ >=
      tracker_->tracker_params_.intra_keyframe_time_ns_;
  const size_t& nr_valid_features = mono_frame_k_->getNrValidKeypoints();
  const bool nr_features_low =
      nr_valid_features <= tracker_->tracker_params_.min_number_features_;

  LOG_IF(WARNING, mono_frame_k_->isKeyframe_) << "User enforced keyframe!";

  if (max_time_elapsed || nr_features_low || mono_frame_k_->isKeyframe_) {
    VLOG(2) << "Keframe after [s]: "
            << UtilsNumerical::NsecToSec(mono_frame_k_->timestamp_ -
                                         last_keyframe_timestamp_);
    ++keyframe_count_;

    VLOG_IF(2, max_time_elapsed) << "Keyframe reason: max time elapsed.";
    VLOG_IF(2, nr_features_low)
        << "Keyframe reason: low nr of features (" << nr_valid_features << " < "
        << tracker_->tracker_params_.min_number_features_ << ").";

    if (tracker_->tracker_params_.useRANSAC_) {
      // XXX: This part is great, it reuses the rotation given by the IMU
      // and solves a RANSAC foe estimating the relative pose difference of the
      // camera since las keyframe
      // the status_pose_mono has the same rotation (keyframe_R_cur_frame) inside
      // plus the translation since the last keyframe
      // it also removes the outlier landmarks from the two frames
      TrackingStatusPose status_pose_mono;
      outlierRejectionMono(keyframe_R_cur_frame, // <<<
                           mono_frame_lkf_.get(),
                           mono_frame_k_.get(),
                           &status_pose_mono);
      // XXX: status_pose_mono.first can be invalid/valid/low_disparity or few_matches
      // XXX: status_pose_mono.second is the tracked (pim rotation + opengv translation) pose
      tracker_status_summary_.kfTrackingStatus_mono_ = status_pose_mono.first;

      if (status_pose_mono.first == TrackingStatus::VALID) {
        tracker_status_summary_.lkf_T_k_mono_ = status_pose_mono.second;
      }
    } else {
      tracker_status_summary_.kfTrackingStatus_mono_ = TrackingStatus::DISABLED;
    }

    if (VLOG_IS_ON(2)) {
      printTrackingStatus(tracker_status_summary_.kfTrackingStatus_mono_,
                          "mono");
    }

    last_keyframe_timestamp_ = mono_frame_k_->timestamp_;
    mono_frame_k_->isKeyframe_ = true;

    CHECK(feature_detector_);
    // XXX: TODO Oh so feature detection is only done in keyframes
    // XXX: After reading it, it does seem quite heavy (let's use a GPU!)
    feature_detector_->featureDetection(mono_frame_k_.get()); // <<<

    // Undistort keypoints:
    // XXX: Fill keypoints_undistorted_, are the same keypoints but undistorted
    // plus they have a VALID or NO_LEFT_RECT status for usage
    // though what I really am skeptical about (performance-wise)
    // is the fact that a very VERY similar undistortion happens for each
    // keypoint at the start in featureTracking.
    mono_camera_->undistortKeypoints(mono_frame_k_->keypoints_, // <<<
                                     &mono_frame_k_->keypoints_undistorted_);
    // Log images if needed.
    // if (logger_ &&
    //     (FLAGS_visualize_frontend_images || FLAGS_save_frontend_images)) {
    //   if (FLAGS_log_feature_tracks) sendFeatureTracksToLogger();
    //   if (FLAGS_log_mono_matching_images) sendMonoTrackingToLogger();
    // }
    // XXX: This is the activated-by-default display
    if (display_queue_ && FLAGS_visualize_feature_tracks) {
      displayImage(mono_frame_k_->timestamp_,
                   "feature_tracks",
                   tracker_->getTrackerImage(*mono_frame_lkf_,
                                             *mono_frame_k_),
                   display_queue_);
    }

    // XXX: of course this k is a keyframe (see above if condition)
    mono_frame_lkf_ = mono_frame_k_;

    start_time = utils::Timer::tic();
    // XXX: smart_mono_measurements are just a list of valid <landmarkId, undistorted keypoint>
    getSmartMonoMeasurements(mono_frame_k_, &smart_mono_measurements);
    double get_smart_mono_meas_time = utils::Timer::toc(start_time).count();

    VLOG(2) << "timeGetMeasurements: " << get_smart_mono_meas_time;
  } else {
    CHECK_EQ(smart_mono_measurements.size(), 0u);
    mono_frame_k_->isKeyframe_ = false;
  }

  if (mono_frame_k_->isKeyframe_) {
    keyframe_R_ref_frame_ = gtsam::Rot3::identity();
  } else {
    keyframe_R_ref_frame_ = keyframe_R_cur_frame;
  }

  mono_frame_km1_ = mono_frame_k_;
  // XXX: reset() shared pointer, nothing more
  mono_frame_k_.reset();
  ++frame_count_;

  // XXX: In summary we will return:
  // tracker_status_summary_ kfStatus will be invalid unless it this is a keyframe
  // tracker_status_Summary_.lkf_T_k_mono_ won't change if not a keyframe
  // but if it is a keyframe it will contain the delta pose between the previous
  // keyframe and this keyframe
  // smart_mono_measurements: will be a list of valid landmarkIds and their
  // 2d undistorted keypoints only on keyframes, else it is a list with size zero
  return std::make_shared<StatusMonoMeasurements>(
    std::make_pair(tracker_status_summary_, smart_mono_measurements));
}

// TODO(marcus): for convenience mono measurements are gtsam::StereoPoint2
// hence this is near identical to getSmartStereoMeasurements
// but want to switch to gtsam::Point2
void MonoVisionImuFrontend::getSmartMonoMeasurements(
    const Frame::Ptr& frame, MonoMeasurements* smart_mono_measurements) {
  // TODO(marcus): convert to point2 when ready!
  CHECK_NOTNULL(smart_mono_measurements);
  frame->checkFrame();

  const LandmarkIds& landmarkId_kf = frame->landmarks_;
  const StatusKeypointsCV& keypoints_undistorted =
      frame->keypoints_undistorted_;

  // Pack information in landmark structure.
  smart_mono_measurements->clear();
  smart_mono_measurements->reserve(landmarkId_kf.size());
  for (size_t i = 0; i < landmarkId_kf.size(); ++i) {
    if (landmarkId_kf.at(i) == -1) {
      continue;  // skip invalid points
    }

    // TODO implicit conversion float to double increases floating-point
    // precision! Using auto instead of double for possible future proofing.
    const auto& uL = keypoints_undistorted.at(i).second.x;
    const auto& v = keypoints_undistorted.at(i).second.y;

    // Initialize to missing pixel information.
    double uR = std::numeric_limits<double>::quiet_NaN();
    smart_mono_measurements->push_back(
        std::make_pair(landmarkId_kf[i], gtsam::StereoPoint2(uL, uR, v)));
  }
}

void MonoVisionImuFrontend::printStatusMonoMeasurements(
    const StatusMonoMeasurements& status_mono_measurements) {
  LOG(INFO) << "SmartMonoMeasurements with status: ";
  printTrackingStatus(status_mono_measurements.first.kfTrackingStatus_mono_,
                      "mono");
  LOG(INFO) << " stereo points:";
  const MonoMeasurements& mono_measurements = status_mono_measurements.second;
  for (const auto& meas : mono_measurements) {
    std::cout << " " << meas.second << " ";
  }
  std::cout << std::endl;
}

}  // namespace VIO
