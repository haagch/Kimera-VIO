// Copyright 2021, Collabora, Ltd.

/**
 * @file   Kimera.cpp
 * @brief  Expose Kimera as a library for external usage
 * @author Mateo de Mayo <mateo.demayo@collabora.com>
 */

#include "kimera-vio/pipeline/Kimera.h"

#include <future>
#include <string>

#include "kimera-vio/common/VioNavState.h"
#include "kimera-vio/dataprovider/EurocDataProvider.h"
#include "kimera-vio/imu-frontend/ImuFrontend-definitions.h"
#include "kimera-vio/pipeline/MonoImuPipeline.h"
#include "kimera-vio/pipeline/Pipeline.h"
#include "kimera-vio/pipeline/StereoImuPipeline.h"

DEFINE_string(
    wrapper_params_folder_path,
    "../params/Euroc",
    "Path to the folder containing the yaml files with the VIO parameters.");

using namespace VIO;

namespace VIO {

Kimera::Kimera(std::string flagfile) {
  google::ReadFromFlagsFile(flagfile, "Kimera", true);
  google::InitGoogleLogging("Kimera");
  LOG(INFO) << "Wrapper params directory: " << FLAGS_wrapper_params_folder_path;

  vio_params_ = std::shared_ptr<VioParams>(
      new VioParams(FLAGS_wrapper_params_folder_path));
  VioParams& vio_params = *vio_params_;

  is_parallel_ = vio_params.parallel_run_;
  is_stereo_ = vio_params.frontend_type_ == FrontendType::kStereoImu;

  if (is_stereo_) {
    vio_pipeline_ = make_unique<StereoImuPipeline>(vio_params);
  } else {
    vio_pipeline_ = make_unique<MonoImuPipeline>(vio_params);
  }

  vio_pipeline_->registerBackendOutputCallback(
      [this](auto o) { this->receivePoseFromBackend(o); });

  vio_pipeline_->registerShutdownCallback(
      [this]() { this->is_running_ = false; });
}

void Kimera::start() {
  is_running_ = true;
  awaiting_left_frame_ = true;

  if (is_parallel_) {
    auto pipe = std::async(std::launch::async, &Pipeline::spin, vio_pipeline_);
    vio_pipeline_->spinViz();
    pipe.get();
  }
}

void Kimera::stop() { vio_pipeline_->shutdown(); }

bool Kimera::isRunning() { return is_running_; }

void Kimera::pushImuSample(std::int64_t t,
                           double ax,
                           double ay,
                           double az,
                           double wx,
                           double wy,
                           double wz) {
  if (!is_running_) return;
  Vector6 accgyr;
  accgyr << ax, ay, az, wx, wy, wz;
  ImuMeasurement im{t, accgyr};
  vio_pipeline_->fillSingleImuQueue(im);
}

void Kimera::pushFrame(std::int64_t t, cv::Mat img, bool is_left) {
  if (!is_running_) return;
  if (img.channels() > 1) {
    LOG(WARNING) << "Converting img from BGR to GRAY...";
    cv::cvtColor(img, img, cv::COLOR_BGR2GRAY);
  }
  CHECK_EQ(awaiting_left_frame_, is_left);
  CHECK(is_stereo_ || is_left) << "Only left frames allowed on a mono pipeline";

  VioParams& vp = *vio_params_;

  bool equalize = vp.frontend_params_.stereo_matching_params_.equalize_image_;
  if (equalize) cv::equalizeHist(img, img);

  const CameraParams& cp = vp.camera_params_.at(is_left ? 0 : 1);
  Frame::UniquePtr frame = nullptr;
  frame = make_unique<Frame>(current_k_, t, cp, img);

  if (is_stereo_) {
    if (is_left) {
      vio_pipeline_->fillLeftFrameQueue(std::move(frame));
    } else {
      StereoImuPipeline::Ptr stereo_pipeline =
          safeCast<Pipeline, StereoImuPipeline>(vio_pipeline_);
      stereo_pipeline->fillRightFrameQueue(std::move(frame));
      current_k_++;
    }
    awaiting_left_frame_ = !awaiting_left_frame_;
  } else {
    vio_pipeline_->fillLeftFrameQueue(std::move(frame));
    current_k_++;
  }

  if (!is_parallel_) vio_pipeline_->spin();
}

bool Kimera::tryDequeuePose(Pose& pose) { return poses_.try_dequeue(pose); }

bool Kimera::tryEnqueuePose(Pose&& pose) { return poses_.try_enqueue(pose); }

void Kimera::receivePoseFromBackend(std::shared_ptr<BackendOutput>& output) {
  gtsam::Pose3 gpose = output->W_State_Blkf_.pose_;
  Eigen::Vector3d pos = gpose.translation();
  Eigen::Quaternion<double, Eigen::DontAlign> rot =
      gpose.rotation().toQuaternion();
  tryEnqueuePose(
      Pose(pos.x(), pos.y(), pos.z(), rot.x(), rot.y(), rot.z(), rot.w()));
}

}  // namespace VIO

namespace xrt::auxiliary::tracking::slam {

struct slam_tracker::implementation {
  // Let's just point to our existing Kimera wrapper class instead of reimplement it
  Kimera *kimera;
};

slam_tracker::slam_tracker(std::string configfile) {
  impl = new slam_tracker::implementation;
  impl->kimera = new Kimera{configfile};
}

slam_tracker::~slam_tracker() {
  delete impl->kimera;
  delete impl;
}

void slam_tracker::start() { impl->kimera->start(); }
void slam_tracker::stop() { impl->kimera->stop(); }
bool slam_tracker::is_running() { return impl->kimera->isRunning(); }

void slam_tracker::push_imu_sample(imu_sample s) {
  impl->kimera->pushImuSample(s.timestamp, s.ax, s.ay, s.az, s.wx, s.wy, s.wz);
}

void slam_tracker::push_frame(img_sample sample) {
  impl->kimera->pushFrame(sample.timestamp, sample.img, sample.is_left);
}

bool slam_tracker::try_dequeue_pose(pose& pose) {
  VIO::Pose pose_k;
  bool ret = impl->kimera->tryDequeuePose(pose_k);
  if (ret) {
    // TODO@mateosss: just replace usage of VIO::Pose for pose everywhere
    // instead of doing this copy nonsense. EDIT: Am I sure of this? doing
    // so would make the Kimera wrapper coupled with Monado, and right now
    // it works very well on its own.
    pose.px = pose_k.px;
    pose.py = pose_k.py;
    pose.pz = pose_k.pz;
    pose.rx = pose_k.rx;
    pose.ry = pose_k.ry;
    pose.rz = pose_k.rz;
    pose.rw = pose_k.rw;
  }
  return ret;
}

}  // namespace xrt::auxiliary::tracking::slam
