// Copyright 2021, Collabora, Ltd.

/**
 * @file   Kimera.h
 * @brief  Expose Kimera as a library for external usage
 * @author Mateo de Mayo <mateo.demayo@collabora.com>
 */

#pragma once

#include <memory>
#include <mutex>
#include <opencv2/core/mat.hpp>

#include "kimera-vio/third_party/readerwriterqueue/readerwriterqueue.h"
#include "kimera-vio/third_party/monado/slam_tracker.hpp"

namespace VIO {

class Pipeline;
class BackendOutput;
class CameraParams;
class VioParams;
class DataProviderInterface;

struct Pose {
  float px, py, pz;
  float rx, ry, rz, rw;
  Pose() = default;
  Pose(float px, float py, float pz, float rx, float ry, float rz, float rw)
      : px(px), py(py), pz(pz), rx(rx), ry(ry), rz(rz), rw(rw) {}
};

class Kimera {
 public:
  Kimera(std::string flagfile);
  void start();
  void stop();
  bool isRunning();

  void pushImuSample(std::int64_t t,
                     double ax,
                     double ay,
                     double az,
                     double wx,
                     double wy,
                     double wz);
  void pushFrame(std::int64_t t, cv::Mat img, bool is_left);

  bool tryDequeuePose(Pose& pose);

 private:
  bool tryEnqueuePose(Pose&& pose);
  void receivePoseFromBackend(std::shared_ptr<BackendOutput>& output);

  std::shared_ptr<Pipeline> vio_pipeline_;
  std::shared_ptr<VioParams> vio_params_;
  std::shared_ptr<DataProviderInterface> dataset_parser_;
  bool is_parallel_;
  bool is_stereo_;

  bool is_running_ = false;
  bool awaiting_left_frame_ = false;  // Forces left-right interleaved pushFrame
  std::uint64_t current_k_ = 0;

  // TODO: Determine a reasonable initial size instead of just 100
  moodycamel::ReaderWriterQueue<Pose> poses_{100};
};

}  // namespace VIO
